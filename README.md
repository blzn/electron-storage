
```bash
# Clone this repository
git clone https://gitlab.com/blzn/electron-storage.git
# Go into the repository
cd electron-quick-start
# Install dependencies
npm install
# Run the app
npm start
```

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
