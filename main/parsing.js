//const { dialog } = require("electron");
const { parentPort, workerData } = require("worker_threads");
const path = require("path");
const XLSX = require("xlsx");
//var sqlite3 = require("sqlite3").verbose();

function parse(filepath) {
  console.log("importing file " + filepath);
  //if (!filepath.includes("xls")) {
  //dialog.showMessageBox({
  //title: "error",
  //message: "invalid type",
  //});
  //return;
  //}

  //var db = new sqlite3.Database(`${path.parse(filepath).name}.db`);

  //db.serialize(function () {
  //db.run("CREATE TABLE if not exists lorem (info TEXT)");

  //var stmt = db.prepare("INSERT INTO lorem VALUES (?)");
  //for (var i = 0; i < 10; i++) {
  //stmt.run("Ipsum " + i);
  //}
  //stmt.finalize();

  //db.each("SELECT rowid AS id, info FROM lorem", function (err, row) {
  //console.log(row.id + ": " + row.info);
  //});
  //});

  //db.close();

  console.time("sheetJS parsing");
  const workbook = XLSX.readFile(filepath);
  //console.log("wb", workbook);
  //console.log(Object.keys(workbook.Sheets.Sheet1));
  //console.log(workbook.Sheets.Sheet1);
  //console.log(Object.keys(workbook.Sheets.Sheet1).length);
  //if (
  //!workbook.Sheets ||
  //!workbook.Sheets.Sheet1 ||
  //!workbook.Sheets.Sheet1["!ref"]
  //) {
  //console.error("hmmmm bizare");
  //}
  //console.log(workbook.Sheets.Sheet1["!ref"]);
  //workbook.Sheets[workbook.SheetNames[0]]["!ref"] = "A1:E6";
  //sheet2arr(workbook.Sheets[workbook.SheetNames[0]])
  const json = XLSX.utils.sheet_to_json(
    workbook.Sheets[workbook.SheetNames[0]],
    {
      header: 1,
    }
  );
  console.timeEnd("sheetJS parsing");
  //console.log(json);
  return json;
}

parentPort.postMessage(parse(workerData.value));
