const dropzone = document.getElementById("dropzone");
dropzone.addEventListener("drop", async (event) => {
  event.preventDefault();
  event.stopPropagation();

  dropzone.classList.remove("hover");
  for (const f of event.dataTransfer.files) {
    console.log("File Path of dragged files: ", f.path);
    //const data = window.parseData(f.path);
    ipcRenderer.invoke("ondragstart", f.path).then((data) => {
      console.log("parsing finished", data);
    });
  }
});

dropzone.addEventListener("dragover", (e) => {
  e.preventDefault();
  e.stopPropagation();
});

dropzone.addEventListener("dragenter", (e) => {
  e.preventDefault();
  e.stopPropagation();
  console.log("File is in the Drop Zone");
  dropzone.classList.add("hover");
});

dropzone.addEventListener("dragleave", (e) => {
  e.preventDefault();
  e.stopPropagation();
  console.log("File has left the Drop Zone");
  dropzone.classList.remove("hover");
});

ipcRenderer.on("XLS_parsed", (event, data) => {
  console.log("data received");
});
